﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace AppOpsDashboard.Controllers
{
    public class HomeController : Controller
    {
        private const string URL = "https://api.applicationinsights.io/v1/apps/{0}/{1}/{2}?{3}";
        private const string ExternalJobRequestAPIKEY = "6ef5du3fzbmuv22csb3ak6yjwsash66e2erjvkpe";
        private const string ExternalJobRequestApplicationID = "c0d948dd-03bf-4792-8611-8f22f92054ad";

        private const string JobStatusServiceAPIKEY = "dd3umeggoepodsswo1eotlxhpwz4pfhep55bzgy4";
        private const string JobStatusServiceApplicationID = "7293ff91-8e4e-4c17-bc2a-f6cb41866975";

        public ActionResult Index()
        {
            GetAzureData(ExternalJobRequestApplicationID, ExternalJobRequestAPIKEY, "metrics", "requests/failed", "interval=PT1H");
            return View();
        }

        public static string GetAzureData(string appid, string apikey, string queryType, string queryPath, string parameterString)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("x-api-key", apikey);

            var req = string.Format(URL, appid, queryType, queryPath, parameterString);

            HttpResponseMessage response = client.GetAsync(req).Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                return response.ReasonPhrase;
            }
        }


    }
}